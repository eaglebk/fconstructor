import 'package:fconstructor/entity/curr_page.dart';
import 'package:fconstructor/entity/page.dart';
import 'package:fconstructor/store/appdata.dart';
import 'package:fconstructor/util.dart';
import 'package:fconstructor/builders/widget_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:logging/logging.dart';

// Class for building page with tree widgets from map json
class DynamicPageBuilder {
  static final Logger logger = Logger('DynamicPageBuilder');

  // Build widget
  Widget build(dynamic json, curPage) {
    logger.info("build function");
    var widget = buildFromMap(json, curPage);
    return widget;
  }

  // Build widget from map json
  Widget buildFromMap(map, CurPage curPage) {
    var pages = map['pages'];
    var widgets;

    for (var page in pages) {
      var _page = new Page()
        ..name = page['name']
        ..listWidgets = getWidgetsForPage(page, curPage);
      appData.mapPages[_page.name] = _page;
    }

    widgets = appData.mapPages.values.first.listWidgets;

    if (curPage.getCurrentPage() != null) {
      appData.mapPages.forEach((key, value) => {
            if (key == curPage.getCurrentPage()) {widgets = value.listWidgets}
          });
    }

    return widgets;
  }

  // Constucting final widget fow page from main container and children widgets
  Widget getWidgetsForPage(page, curPage) {
    var _children = <Widget>[
      getContainer(page),
    ];

    _children.addAll(getChildrenWidget(page, curPage));
    return Stack(
      children: _children,
    );
  }

  // Getting children widgets for page
  List<Widget> getChildrenWidget(page, curPage) {
    List<Widget> lw = [];
    var childs = page['childs'];

    for (var child in childs) {
      lw.add(DynamicWidgetBuilder.buildFromMap(child, curPage));
    }
    return lw;
  }
}

// Main container for home screen
Widget getContainer(page) {
  return Container(
    width: double.infinity,
    height: double.infinity,
    color: page.containsKey('backgroundColor')
        ? parseHexColor(page['backgroundColor'])
        : Colors.white,
  );
}
