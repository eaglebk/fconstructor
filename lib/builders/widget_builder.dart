import 'package:fconstructor/entity/curr_page.dart';
import 'package:fconstructor/entity/widgets/button_widget.dart';
import 'package:fconstructor/entity/widgets/dropdown_button.dart';
import 'package:fconstructor/entity/widgets/text_widget.dart';
import 'package:flutter/widgets.dart';

// Widget Builder class
class DynamicWidgetBuilder {
  // list of widget parsers
  static final parsers = [
    RaisedButtonParser(),
    TextParser(),
    DropDownParser(),
  ];

  // Build widget from json structure
  static Widget buildFromMap(dynamic map, curPage) {
    for (var _key in map.keys) {
      for (var parser in parsers) {
        if (parser.forWidget(_key)) {
          return parser.parse(map.values.first, curPage);
        }
      }
    }
    return null;
  }

  // Build widgets list
  static List<Widget> buildWidgets(List<dynamic> values, curPage) {
    List<Widget> rt = [];
    for (var value in values) {
      rt.add(buildFromMap(value, curPage));
    }
    return rt;
  }
}

// Abstract class - widget parser
abstract class WidgetParser {
  /// parse the json map into a flutter widget.
  Widget parse(Map<String, dynamic> map, CurPage curPage);

  bool forWidget(String widgetName);
}
