import 'package:fconstructor/store/appdata.dart';
import 'package:http/http.dart' as http;

// Util class for http feature
class HttpHelper {
  static Future<String> get getJsonConfig async {
    var _json;
    final response = await http.get('http://${appData.ip}:${appData.port}/db')
    .timeout(const Duration(seconds: 10));
      _json = response.body;
      print("Response body: ${response.statusCode}");

    if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    return _json;
  } else {
    // If that call was not successful, throw an error. 
    throw Exception('Failed to load post');
  }
  // A timeout occurred. 
  }
}
