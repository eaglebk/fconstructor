
// Json structure - ui configuration

var bootstrapJson = '''

{
  "pages": [
    {
      "name": "Page 1",
      "backgroundColor": "#00FF00",
      "childs": [
        {
          "dropdownButton": {
            "x": 0.50,
            "y": 0.95,
            "z": 0,
            "width": 150,
            "height": 32,
            "value": "Page 1",
            "fontColor": "#493519",
            "fontSize": 20,
            "hint": "select item",
            "triangleSize": 20,
            "items": [
              {
                "dropdownMenuItem": {
                  "value": "Page 1",
                  "child": [
                    {
                      "text": {
                        "text": "go to page 1",
                        "textSize": 15,
                        "textColor": "#493519"
                      }
                    }
                  ]
                }
              },
              {
                "dropdownMenuItem": {
                  "value": "Page 2",
                  "child": [
                    {
                      "text": {
                        "text": "go to page 2",
                        "textSize": 15,
                        "textColor": "#493519"
                      }
                    }
                  ]
                }
              },
              {
                "dropdownMenuItem": {
                  "value": "Page 3",
                  "child": [
                    {
                      "text": {
                        "text": "go to page 3",
                        "textSize": 15,
                        "textColor": "#493519"
                      }
                    }
                  ]
                }
              },
              {
                "dropdownMenuItem": {
                  "value": "Page 4",
                  "child": [
                    {
                      "text": {
                        "text": "go to page 4",
                        "textSize": 15,
                        "textColor": "#493519"
                      }
                    }
                  ]
                }
              },
              {
                "dropdownMenuItem": {
                  "value": "Page 5",
                  "child": [
                    {
                      "text": {
                        "text": "go to page 5",
                        "textSize": 15,
                        "textColor": "#493519"
                      }
                    }
                  ]
                }
              }
            ]
          }
        },
        {
          "button": {
            "x": 0.86,
            "y": 0.47,
            "z": 0,
            "width": 150,
            "height": 32,
            "buttonImage": "",
            "backgroundColor": "#FFFF00",
            "text": "go to page 2",
            "textColor": "#493519",
            "iconColor": "#5DA017",
            "functionName": "navigator.goTo(Page 2)",
            "alignmentIcon": "centerRight",
            "icon": "assets/images/arrow_right.svg"
          }
        },
        {
          "text": {
            "x": 0.5032258064516129,
            "y": 0.09886234357224119,
            "z": 0,
            "width": 200,
            "height": 50,
            "text": "nvxTutorial_Mobile_6Pages",
            "textSize": 15,
            "textColor": "#493519"
          }
        },
        {
          "text": {
            "x": 0.496774193548387,
            "y": 0.30830489192263943,
            "z": 0,
            "width": 200,
            "height": 100,
            "text": "Page 1",
            "textSize": 50,
            "textColor": "#493519"
          }
        },
        {
          "text": {
            "x": 0.5053763440860215,
            "y": 0.60,
            "z": 0,
            "width": 200,
            "height": 60,
            "text": "footer text",
            "textColor": "#493519"
          }
        }
      ]
    },
    {
      "name": "Page 2",
      "backgroundColor": "#FFFFFF",
      "childs": [
        {
          "dropdownButton": {
            "x": 0.50,
            "y": 0.95,
            "z": 0,
            "width": 150,
            "height": 32,
            "value": "Page 1",
            "fontColor": "#493519",
            "fontSize": 20,
            "hint": "select item",
            "triangleSize": 20,
            "items": [
              {
                "dropdownMenuItem": {
                  "value": "Page 1",
                  "child": [
                    {
                      "text": {
                        "text": "go to page 1",
                        "textSize": 15,
                        "textColor": "#493519"
                      }
                    }
                  ]
                }
              },
              {
                "dropdownMenuItem": {
                  "value": "Page 3",
                  "child": [
                    {
                      "text": {
                        "text": "go to page 3",
                        "textSize": 15,
                        "textColor": "#493519"
                      }
                    }
                  ]
                }
              },
              {
                "dropdownMenuItem": {
                  "value": "Page 4",
                  "child": [
                    {
                      "text": {
                        "text": "go to page 4",
                        "textSize": 15,
                        "textColor": "#493519"
                      }
                    }
                  ]
                }
              }
            ]
          }
        },
        {
          "button": {
            "x": 0.1,
            "y": 0.46772468714448234,
            "z": 0,
            "width": 150,
            "height": 32,
            "buttonImage": "",
            "text": "go to level 1",
            "textColor": "#493519",
            "backgroundColor": "#FFFF00",
            "functionName": "navigator.goTo(Page 1)",
            "alignmentIcon": "centerLeft",
            "icon": "assets/images/arrow_left.svg",
            "iconColor": "#5DA017"
          }
        },
        {
          "button": {
            "x": 0.86,
            "y": 0.47,
            "z": 0,
            "width": 150,
            "height": 32,
            "buttonImage": "",
            "backgroundColor": "#FFFF00",
            "text": "go to level 3",
            "textColor": "#493519",
            "iconColor": "#5DA017",
            "functionName": "navigator.goTo(Page 3)",
            "alignmentIcon": "centerRight",
            "icon": "assets/images/arrow_right.svg"
          }
        },
        {
          "text": {
            "x": 0.5032258064516129,
            "y": 0.09886234357224119,
            "z": 0,
            "width": 200,
            "height": 50,
            "text": "nvxTutorial_Mobile_6Pages",
            "textSize": 15,
            "textColor": "#493519"
          }
        },
        {
          "text": {
            "x": 0.496774193548387,
            "y": 0.30830489192263943,
            "z": 0,
            "width": 200,
            "height": 100,
            "text": "Page 2",
            "textSize": 50,
            "textColor": "#493519"
          }
        },
        {
          "text": {
            "x": 0.5053763440860215,
            "y": 0.6382252559726963,
            "z": 0,
            "width": 200,
            "height": 50,
            "text": "MobiCall@NewVoice.ch",
            "textColor": "#493519"
          }
        }
      ]
    },
    {
      "name": "Page 3",
      "backgroundColor": "#FFFF00",
      "childs": [
        {
          "button": {
            "x": 0.1,
            "y": 0.46772468714448234,
            "z": 0,
            "width": 150,
            "height": 32,
            "buttonImage": "",
            "text": "go to level 2",
            "textColor": "#493519",
            "backgroundColor": "#FFFF00",
            "functionName": "navigator.goTo(Page 2)",
            "alignmentIcon": "centerLeft",
            "icon": "assets/images/arrow_left.svg",
            "iconColor": "#5DA017"
          }
        },
        {
          "button": {
            "x": 0.86,
            "y": 0.47,
            "z": 0,
            "width": 150,
            "height": 32,
            "buttonImage": "",
            "backgroundColor": "#FFFF00",
            "text": "go to level 4",
            "textColor": "#493519",
            "iconColor": "#5DA017",
            "functionName": "navigator.goTo(Page 4)",
            "alignmentIcon": "centerRight",
            "icon": "assets/images/arrow_right.svg"
          }
        },
        {
          "text": {
            "x": 0.5032258064516129,
            "y": 0.09886234357224119,
            "z": 0,
            "width": 200,
            "height": 50,
            "text": "nvxTutorial_Mobile_6Pages",
            "textSize": 15,
            "textColor": "#493519"
          }
        },
        {
          "text": {
            "x": 0.496774193548387,
            "y": 0.30830489192263943,
            "z": 0,
            "width": 200,
            "height": 100,
            "text": "Page 3",
            "textSize": 50,
            "textColor": "#493519"
          }
        },
        {
          "text": {
            "x": 0.5053763440860215,
            "y": 0.6382252559726963,
            "z": 0,
            "width": 200,
            "height": 50,
            "text": "MobiCall@NewVoice.ch",
            "textColor": "#493519"
          }
        }
      ]
    },
    {
      "name": "Page 4",
      "backgroundColor": "#4388FE",
      "childs": [
        {
          "button": {
            "x": 0.1,
            "y": 0.46772468714448234,
            "z": 0,
            "width": 150,
            "height": 32,
            "buttonImage": "",
            "text": "go to level 3",
            "textColor": "#493519",
            "backgroundColor": "#FFFF00",
            "functionName": "navigator.goTo(Page 3)",
            "alignmentIcon": "centerLeft",
            "icon": "assets/images/arrow_left.svg",
            "iconColor": "#5DA017"
          }
        },
        {
          "button": {
            "x": 0.86,
            "y": 0.47,
            "z": 0,
            "width": 150,
            "height": 32,
            "buttonImage": "",
            "backgroundColor": "#FFFF00",
            "text": "go to level 5",
            "textColor": "#493519",
            "iconColor": "#5DA017",
            "functionName": "navigator.goTo(Page 5)",
            "alignmentIcon": "centerRight",
            "icon": "assets/images/arrow_right.svg"
          }
        },
        {
          "text": {
            "x": 0.5032258064516129,
            "y": 0.09886234357224119,
            "z": 0,
            "width": 200,
            "height": 50,
            "text": "nvxTutorial_Mobile_6Pages",
            "textSize": 15,
            "textColor": "#493519"
          }
        },
        {
          "text": {
            "x": 0.496774193548387,
            "y": 0.30830489192263943,
            "z": 0,
            "width": 200,
            "height": 100,
            "text": "Page 4",
            "textSize": 50,
            "textColor": "#493519"
          }
        },
        {
          "text": {
            "x": 0.5053763440860215,
            "y": 0.6382252559726963,
            "z": 0,
            "width": 200,
            "height": 50,
            "text": "MobiCall@NewVoice.ch",
            "textColor": "#493519"
          }
        }
      ]
    },
    {
      "name": "Page 5",
      "backgroundColor": "#FF8800",
      "childs": [
        {
          "dropdownButton": {
            "x": 0.50,
            "y": 0.95,
            "z": 0,
            "width": 150,
            "height": 32,
            "value": "Page 1",
            "fontColor": "#493519",
            "fontSize": 20,
            "hint": "select item",
            "triangleSize": 20,
            "items": [
              {
                "dropdownMenuItem": {
                  "value": "Page 1",
                  "child": [
                    {
                      "text": {
                        "text": "go to page 1",
                        "textSize": 15,
                        "textColor": "#493519"
                      }
                    }
                  ]
                }
              },
              {
                "dropdownMenuItem": {
                  "value": "Page 2",
                  "child": [
                    {
                      "text": {
                        "text": "go to page 2",
                        "textSize": 15,
                        "textColor": "#493519"
                      }
                    }
                  ]
                }
              },
              {
                "dropdownMenuItem": {
                  "value": "Page 3",
                  "child": [
                    {
                      "text": {
                        "text": "go to page 3",
                        "textSize": 15,
                        "textColor": "#493519"
                      }
                    }
                  ]
                }
              }
            ]
          }
        },
        {
          "button": {
            "x": 0.1,
            "y": 0.46772468714448234,
            "z": 0,
            "width": 150,
            "height": 32,
            "buttonImage": "",
            "text": "go to level 4",
            "textColor": "#493519",
            "backgroundColor": "#FFFF00",
            "functionName": "navigator.goTo(Page 4)",
            "alignmentIcon": "centerLeft",
            "icon": "assets/images/arrow_left.svg",
            "iconColor": "#5DA017"
          }
        },
        {
          "button": {
            "x": 0.86,
            "y": 0.47,
            "z": 0,
            "width": 150,
            "height": 32,
            "buttonImage": "",
            "backgroundColor": "#FFFF00",
            "text": "go to level 6",
            "textColor": "#493519",
            "iconColor": "#5DA017",
            "functionName": "navigator.goTo(Page 6)",
            "alignmentIcon": "centerRight",
            "icon": "assets/images/arrow_right.svg"
          }
        },
        {
          "text": {
            "x": 0.5032258064516129,
            "y": 0.09886234357224119,
            "z": 0,
            "width": 200,
            "height": 50,
            "text": "nvxTutorial_Mobile_6Pages",
            "textSize": 15,
            "textColor": "#493519"
          }
        },
        {
          "text": {
            "x": 0.496774193548387,
            "y": 0.30830489192263943,
            "z": 0,
            "width": 200,
            "height": 100,
            "text": "Page 5",
            "textSize": 50,
            "textColor": "#493519"
          }
        },
        {
          "text": {
            "x": 0.5053763440860215,
            "y": 0.6382252559726963,
            "z": 0,
            "width": 200,
            "height": 50,
            "text": "MobiCall@NewVoice.ch",
            "textColor": "#493519"
          }
        }
      ]
    },
    {
      "name": "Page 6",
      "backgroundColor": "#43CEFF",
      "childs": [
        {
          "button": {
            "x": 0.1,
            "y": 0.46772468714448234,
            "z": 0,
            "width": 150,
            "height": 32,
            "buttonImage": "",
            "text": "go to level 5",
            "textColor": "#493519",
            "backgroundColor": "#FFFF00",
            "functionName": "navigator.goTo(Page 5)",
            "alignmentIcon": "centerLeft",
            "icon": "assets/images/arrow_left.svg",
            "iconColor": "#5DA017"
          }
        },
        {
          "button": {
            "x": 0.86,
            "y": 0.47,
            "z": 0,
            "width": 150,
            "height": 32,
            "buttonImage": "",
            "backgroundColor": "#FFFF00",
            "text": "go to page 1",
            "textColor": "#493519",
            "iconColor": "#5DA017",
            "functionName": "navigator.goTo(Page 1)",
            "alignmentIcon": "centerRight",
            "icon": "assets/images/arrow_right.svg"
          }
        },
        {
          "text": {
            "x": 0.5032258064516129,
            "y": 0.09886234357224119,
            "z": 0,
            "width": 200,
            "height": 50,
            "text": "nvxTutorial_Mobile_6Pages",
            "textSize": 15,
            "textColor": "#493519"
          }
        },
        {
          "text": {
            "x": 0.496774193548387,
            "y": 0.30830489192263943,
            "z": 0,
            "width": 250,
            "height": 100,
            "text": "Finish Page!",
            "textSize": 40,
            "textColor": "#0000FF"
          }
        },
        {
          "text": {
            "x": 0.5053763440860215,
            "y": 0.6382252559726963,
            "z": 0,
            "width": 200,
            "height": 50,
            "text": "MobiCall@NewVoice.ch",
            "textColor": "#493519"
          }
        }
      ]
    }
  ],
  "navigator": {
    "routes": [
      "/Page1",
      "Page2",
      "Page3",
      "Page4",
      "Page5",
      "Page6"
    ],
    "navigates": [
      {
        "goToPage1": "/Page1"
      },
      {
        "goToPage6": "/Page6"
      },
      {
        "goHome": "home"
      },
      {
        "goToEnd": "/Page6"
      }
    ]
  },
  "actionManager": {
    "actions": [
      {
        "alarm": "6001"
      },
      {
        "alarm": "6002"
      }
    ]
  }
}

''';