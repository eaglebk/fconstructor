import 'package:flutter/widgets.dart';

// Entity class for current page
class CurPage with ChangeNotifier {
  String _cP;

  CurPage(this._cP);

  getCurrentPage() => _cP;

  setCurrentPage(String cP) {
    _cP = cP;
    notifyListeners();
  }
}