import 'package:fconstructor/builders/widget_builder.dart';
import 'package:fconstructor/entity/curr_page.dart';
import 'package:fconstructor/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

// Button (with icon or without it) widget parser
class RaisedButtonParser extends WidgetParser {
  @override
  bool forWidget(String widgetName) {
    return "button" == widgetName;
  }

  @override
  Widget parse(Map<String, dynamic> map, CurPage curPage) {
    var alignmentX = map.containsKey('x') ? map['x'].toDouble() : 0.0;
    var alignmentY = map.containsKey('y') ? map['y'].toDouble() : 0.0;
    var text = map.containsKey('text') ? map['text'] : "";
    var functionName =
        map.containsKey('functionName') ? map['functionName'] : null;
    var alignmentIcon = map.containsKey('alignmentIcon')
        ? parseAlignment(map['alignmentIcon'])
        : Alignment.centerRight;
    var icon = map.containsKey('icon') ? map['icon'] : null;
    var backgroundColor = map.containsKey('backgroundColor')
        ? parseHexColor(map['backgroundColor'])
        : Colors.transparent;
    var textColor = map.containsKey('textColor')
        ? parseHexColor(map['textColor'])
        : Colors.black;
    var iconColor = map.containsKey('iconColor')
        ? parseHexColor(map['iconColor'])
        : Colors.black;

    var raisedButton = GestureDetector(
      onTap: () => null,
      child: Align(
        alignment: FractionalOffset(alignmentX, alignmentY),
        child: Container(
          width: map.containsKey('width') ? map['width'].toDouble() : 0.0,
          height: map.containsKey('height') ? map['height'].toDouble() : 0.0,
          padding: const EdgeInsets.all(0.0),
          decoration: BoxDecoration(
            color: backgroundColor,
          ),
          child: icon == null
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      text,
                      style: TextStyle(fontSize: 14),
                    ),
                  ],
                )
              : getButtonWithIcon(icon, backgroundColor, text, alignmentIcon,
                  textColor, iconColor, functionName, curPage),
        ),
      ),
    );

    return raisedButton;
  }

  // Getting button with icon
  Widget getButtonWithIcon(icon, backgroundColor, text, alignmentIcon,
      textColor, iconColor, functionName, CurPage curPage) {
    Container container;

    if (icon != null) {
      return RaisedButton(
        onPressed: () => curPage.setCurrentPage(getfuncParam(functionName)),
        child: Stack(
          children: <Widget>[
            Align(
                alignment: alignmentIcon,
                child: icon.contains(new RegExp(r'png|jpeg|jpg'))
                    ? Image.asset(icon)
                    : SvgPicture.asset(
                        icon,
                        color: iconColor,
                        width: 20,
                        height: 20,
                      )),
            Align(
                alignment: Alignment.center,
                child: Text(
                  text,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: textColor,
                  ),
                ))
          ],
        ),
        color: backgroundColor,
      );
    }
    return container;
  }
}
