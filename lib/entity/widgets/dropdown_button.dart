import 'package:fconstructor/builders/widget_builder.dart';
import 'package:fconstructor/entity/curr_page.dart';
import 'package:fconstructor/util.dart';
import 'package:flutter/material.dart';

// DropDown widget parser
class DropDownParser extends WidgetParser {
  @override
  bool forWidget(String widgetName) {
    return "dropdownButton" == widgetName;
  }

  @override
  Widget parse(Map<String, dynamic> map, CurPage curPage) {
    var alignmentX = map.containsKey('x') ? map['x'].toDouble() : 0.0;
    var alignmentY = map.containsKey('y') ? map['y'].toDouble() : 0.0;
    var width = map.containsKey('width') ? map['width'].toDouble() : 0.0;
    var height = map.containsKey('height') ? map['height'].toDouble() : 0.0;
    // TODO add action for selected item
    var functionOnChanged =
        map.containsKey('functionOnChanged') ? map['functionOnChanged'] : null;
    var value = map.containsKey('value') ? map['value'] : null;
    var hint = map.containsKey('hint') ? map['hint'] : "";
    var textSize =
        map.containsKey('fontSize') ? map['fontSize'].toDouble() : 10.0;
    var triangleSize =
        map.containsKey('triangleSize') ? map['triangleSize'].toDouble() : 10.0;
    var textColor = map.containsKey('fontColor')
        ? parseHexColor(map['fontColor'])
        : Colors.black;

    var dropdown = Align(
        alignment: FractionalOffset(alignmentX, alignmentY),
        child: Container(
          width: width,
          height: height,
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: DropdownButton(
            value: null,
            iconSize: triangleSize,
            hint: Text(hint),
            style: TextStyle(
                fontSize: textSize,
                fontWeight: FontWeight.normal,
                color: textColor),
            onChanged: (value) => curPage.setCurrentPage(value),
            items: getItems(map['items'], curPage),
          ),
        ));
    return dropdown;
  }

// Get items for dropdownbutton
  List<DropdownMenuItem<String>> getItems(items, CurPage curPage) {
     var listMenuItem = new List<DropdownMenuItem<String>>();

    for (var _item in items) {
      var map = _item.values.first;

      for (var child in map['child']) {
        var dropdownItem = DropdownMenuItem<String>(
          value: map['value'],
          child: DynamicWidgetBuilder.buildFromMap(child, curPage),
        );

        listMenuItem.add(dropdownItem);
      }
    }

    return listMenuItem;
  }
}
