import 'package:fconstructor/builders/widget_builder.dart';
import 'package:fconstructor/entity/curr_page.dart';
import 'package:fconstructor/util.dart';
import 'package:flutter/material.dart';

// Text widget parser
class TextParser extends WidgetParser {
  @override
  bool forWidget(String widgetName) {
    return "text" == widgetName;
  }

  @override
  Widget parse(Map<String, dynamic> map, CurPage curPage) {
    String data = map['text'];
    var alignmentX = map.containsKey('x') ? map['x'].toDouble() : 0.0;
    var alignmentY = map.containsKey('y') ? map['y'].toDouble() : 0.0;
    var width = map.containsKey('width') ? map['width'].toDouble() : 0.0;
    var height = map.containsKey('height') ? map['height'].toDouble() : 0.0;
    var textSize =
        map.containsKey('textSize') ? map['textSize'].toDouble() : 10.0;
    var textColor = map.containsKey('textColor')
        ? parseHexColor(map['textColor'])
        : Colors.black;
    var backgroundColor = map.containsKey('backgroundColor')
        ? parseHexColor(map['backgroundColor'])
        : Colors.transparent;
    var text;

    if (map.containsKey('x')) {
      text = Align(
          alignment: FractionalOffset(alignmentX, alignmentY),
          child: Container(
            decoration: BoxDecoration(color: backgroundColor),
            width: width,
            height: height,
            child: Text(
              data,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  fontSize: textSize,
                  fontWeight: FontWeight.normal,
                  backgroundColor: backgroundColor,
                  color: textColor),
            ),
          ));
    } else {
      text = Text(
        data,
        textAlign: TextAlign.center,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            fontSize: textSize,
            fontWeight: FontWeight.normal,
            backgroundColor: backgroundColor,
            color: textColor),
      );
    }
    return text;
  }
}
