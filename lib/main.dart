import 'dart:async';
import 'dart:convert';

import 'package:fconstructor/builders/page_builder.dart';
import 'package:fconstructor/data/http_helper.dart';
import 'package:fconstructor/store/appdata.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'entity/curr_page.dart';

// Start point for application
void main() => runApp(MyApp());

// Main class
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: ChangeNotifierProvider<CurPage>(
        builder: (_) => CurPage(""),
        child: Container(
          color: Colors.white,
          child: Stack(
            children: <Widget>[
              Home(),
            ],
          ),
        ),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}

// Home screen
class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

// State for home screen
class _HomeState extends State<Home> {
  var _jsonConfig;
  // TextField controllers for ip, port feilds
  TextEditingController _ipaddressController = TextEditingController();
  TextEditingController _portController = TextEditingController();

  // Timer for updating state by time (6 seconds)
  Timer timer;
  @override
  void initState() {
    super.initState();

    // Start timer
    _startTimer();
  }

  // Clear state for home screen. For memory leak prevention
  @override
  void dispose() {
    super.dispose();
  }

  // Show dialog when the connection was suddenly lost
  _showDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // Return object of type Dialog
        return AlertDialog(
          title: Text('Timeout connection'),
          content: Column(
            children: <Widget>[
              TextField(
                keyboardType: TextInputType.number,
                controller: _ipaddressController,
                decoration:
                    // Hint text for textfield
                    InputDecoration(hintText: "Ip address: Eg: 192.168.1.106"),
              ),
              TextField(
                keyboardType: TextInputType.number,
                controller: _portController,
                // Hint text for textfield
                decoration: InputDecoration(hintText: "Port: Eg: 3000"),
              ),
            ],
          ),
          actions: <Widget>[
            new FlatButton(
              child: new Text("Save"),
              onPressed: () {
                // Saving settings for connection
                appData.ip = _ipaddressController.text;
                appData.port = _portController.text;

                // Re-connection and getting configuration from server
                _startTimer();

                // Hide the dialog
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  // Building widgets tree by structure
  @override
  Widget build(BuildContext context) {
    // Store for variable: current selected page
    final curPage = Provider.of<CurPage>(context);
    return Material(
      type: MaterialType.transparency,
      child: Container(
        alignment: Alignment.center,
        // Future builder for async constructing widgets tree
        child: FutureBuilder<Widget>(
          future: _buildWidgetPage(curPage),
          builder: (BuildContext context, AsyncSnapshot<Widget> snapshot) {
            // If error - print error message in the console
            if (snapshot.hasError) {
              print(snapshot.error);
            }
            return snapshot.hasData
                ? SizedBox.expand(
                    child: snapshot.data,
                  )
                : Text("Loading...");
          },
        ),
      ),
    );
  }

  // Building widget. It takes current selected page and return widgets tree.
  Future<Widget> _buildWidgetPage(CurPage curPage) async {
    return DynamicPageBuilder().build(_jsonConfig, curPage);
  }

  // Getting config structure for building ui and updating the state widget
  _getConfigFromRemote() async {
    try {
      var value = await HttpHelper.getJsonConfig;
      _jsonConfig = jsonDecode(value);
      setState(() {});
    } catch (e) {
      // Else - show network setting dialog
      _showDialog(context);
      timer.cancel();
    }
  }

  // Starting the timer for receiving structure
  void _startTimer() {
    timer = Timer.periodic(
        Duration(seconds: 6), (Timer t) => _getConfigFromRemote());
  }
}

// TODO Action handler

// action(String functionName) {
//   print("Activate action");
//   if (functionName != null) {
//     var funcName = functionName.substring(0, functionName.lastIndexOf('('));
//     var funcParam = functionName.substring(
//         functionName.lastIndexOf('(') + 1, functionName.lastIndexOf(')'));
//     switch (funcName) {
//       case "navigator.goTo":
//         appData.page = funcParam;
//         print("Change page to ${appData.page}");
//         break;
//       case "ActionManager.alarm":
//         print("Action manager activate");
//         break;
//       default:
//         print("Default action");
//     }
//   }
// }
