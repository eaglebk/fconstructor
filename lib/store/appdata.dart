import 'package:flutter/widgets.dart';

// Class store for application
class AppData {

  // Singelton appData
  static final AppData _appData = new AppData._internal();

  // Map structure for pages
  var mapPages = new Map();

  // Variable for storing page with notifying subscribers
  final cP = ValueNotifier("");

  // Default value for ip address
  String ip = "192.168.1.106";
  // Default value for port
  String port = "3000";

  // Constructor for AppData
  factory AppData() {
    return _appData;
  }
  AppData._internal();

  // Remove and dispose listener
  void removeListener(action) {
    cP.removeListener(action);
    cP.dispose();
  }

  // Subscribe by current page value
  void listenCurPageChange(_callback) {
    cP.addListener(_callback);
  }
}

final appData = AppData();
