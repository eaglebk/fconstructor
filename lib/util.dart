import 'dart:ui';

import 'package:flutter/widgets.dart';

// Util class

// Parse color value from string to color
Color parseHexColor(String hexColorString) {
  if (hexColorString == null) {
    return null;
  }

  if (hexColorString.trim().isEmpty) {
    return null;
  }
  hexColorString = hexColorString.toUpperCase().replaceAll("#", "");
  if (hexColorString.length == 6) {
    hexColorString = "FF" + hexColorString;
  }
  int colorInt = int.parse(hexColorString, radix: 16);
  return Color(colorInt);
}

// Parse alignment value for widgets
Alignment parseAlignment(String alignmentString) {
  Alignment alignment = Alignment.topLeft;
  switch (alignmentString) {
    case 'topLeft':
      alignment = Alignment.topLeft;
      break;
    case 'topCenter':
      alignment = Alignment.topCenter;
      break;
    case 'topRight':
      alignment = Alignment.topRight;
      break;
    case 'centerLeft':
      alignment = Alignment.centerLeft;
      break;
    case 'center':
      alignment = Alignment.center;
      break;
    case 'centerRight':
      alignment = Alignment.centerRight;
      break;
    case 'bottomLeft':
      alignment = Alignment.bottomLeft;
      break;
    case 'bottomCenter':
      alignment = Alignment.bottomCenter;
      break;
    case 'bottomRight':
      alignment = Alignment.bottomRight;
      break;
  }
  return alignment;
}

// Get page number from functionName
String getfuncParam(functionName) => functionName.substring(
    functionName.lastIndexOf('(') + 1, functionName.lastIndexOf(')'));
